﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Ancillaries" Type="Folder">
			<Item Name="BorderlessPicture.ctl" Type="VI" URL="../Ancillaries/BorderlessPicture.ctl"/>
			<Item Name="ClassAttributesCluster.ctl" Type="VI" URL="../Ancillaries/ClassAttributesCluster.ctl"/>
			<Item Name="CloseProjectOrLibrary.vi" Type="VI" URL="../Ancillaries/CloseProjectOrLibrary.vi"/>
			<Item Name="ControlAttributesCluster.ctl" Type="VI" URL="../Ancillaries/ControlAttributesCluster.ctl"/>
			<Item Name="FlattenProjectItemsTree.vi" Type="VI" URL="../Ancillaries/FlattenProjectItemsTree.vi"/>
			<Item Name="GlobalElementProcessed.vi" Type="VI" URL="../Ancillaries/GlobalElementProcessed.vi"/>
			<Item Name="ProjectItemsStatistics.vi" Type="VI" URL="../Ancillaries/ProjectItemsStatistics.vi"/>
			<Item Name="SortWithIndices.vim" Type="VI" URL="../Ancillaries/SortWithIndices.vim"/>
			<Item Name="VIattributesCluster.ctl" Type="VI" URL="../Ancillaries/VIattributesCluster.ctl"/>
			<Item Name="VIitemAttributes.vi" Type="VI" URL="../Ancillaries/VIitemAttributes.vi"/>
		</Item>
		<Item Name="ProjectList.vi" Type="VI" URL="../ProjectList.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Draw Flattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Flattened Pixmap.vi"/>
				<Item Name="FixBadRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/FixBadRect.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
			</Item>
			<Item Name="LVLastCompiledWithTypeDef.ctl" Type="VI" URL="../Ancillaries/LVLastCompiledWithTypeDef.ctl"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
